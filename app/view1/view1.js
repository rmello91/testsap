  'use strict';

  angular.module('myApp.view1', ['ngRoute', 'nvd3ChartDirectives'])
  .config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/view1', {
      templateUrl: 'view1/view1.html',
      controller: 'View1Ctrl'
    });
  }])
  .controller('View1Ctrl', ['$scope','$http', function($scope, $http) {
    $http.get('components/json/nets-sales.json').success(function(data) {
      $scope.countries = data;
      $scope.countries[0].selected = true;
      $scope.country_data = $scope.countries[0].country_data;
      $scope.yearly_distribution = $scope.countries[0].country_data.yearly_distribution;

      $scope.graphData = [{}];
      $scope.graphData[0].values = [];
      for(var i = 0; i < $scope.yearly_distribution.length; i++){
        $scope.graphData[0].values.push([$scope.yearly_distribution[i].quarter, parseInt($scope.yearly_distribution[i].sales)]);
      }
    });

    $scope.changeCountry = function(index){
      for(var i = 0; i < $scope.countries.length; i++){
        $scope.countries[i].selected = false;
      }
      $scope.countries[index].selected = true;
      $scope.country_data = $scope.countries[index].country_data;
      $scope.yearly_distribution = $scope.countries[index].country_data.yearly_distribution;

      $scope.graphData = [{}];
      $scope.graphData[0].values = [];
      for(var i = 0; i < $scope.yearly_distribution.length; i++){
        $scope.graphData[0].values.push([$scope.yearly_distribution[i].quarter, parseInt($scope.yearly_distribution[i].sales)]);
      }

    };



  }]);
